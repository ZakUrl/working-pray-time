import time as tm
import datetime as dt
from threading import Thread

prayerNames = { 0 : "Fajr", 1 : "Chourouq", 2 : "Dhor", 3 : "Asr", 4 : "Maghreb", 5 : "Ichaa"}

class Parser:

	def __init__(self,calendar,csv_file):
		self._prayerTime = open(csv_file)
		self._calendar = calendar
		self.parse_header()
		self.parse()

		
	def parse_header(self):
		first_line = self._prayerTime.readline()
		first_line = first_line.split(";")
		self.month = Month()
		self.month.set_month(first_line[0])
		self.month.set_monthLunar(first_line[8])
	
	def parse(self):
		beg_ind_prayer = 2
		for line in self._prayerTime:
			line = line.split(";")
			#creation of a new day because new line
			new_day = Day()
			new_day.set_dayName(line[0])     #monday,tuesday....
			new_day.set_date_sol(line[1])   # number of the day
			#setting pray hours in a day
			for i in range(beg_ind_prayer,beg_ind_prayer+len(prayerNames)):
				t = Time()
				h,m = line[i].strip(" ").split(":")
				t.set_hour(int(h))
				t.set_min(int(m))
				new_day.set_prayers(t)
			new_day.set_date_lun(line[8])
			self.month.add_day(new_day)
		self._calendar.add_month(self.month)



class Calendar:

	def __init__(self):
		#self._year = None
		self._listOfMonth = []
		self.set_today_date()

	def set_today_date(self):
		self.day,self.month,self.year = str(dt.date.today()).split("-")[::-1]
		self.day,self.month,self.year = int(self.day),int(self.month),int(self.year)

	def add_month(self,month):
		self._listOfMonth.append(month)

	def get_months(self):
		return self._listOfMonth

	def get_today_prayer(self):
		prayers = self._listOfMonth[self.month-1].get_listOfDays()[self.day-1].get_prayers()
		return prayers
	
	def get_today_date_lun(self):
		day_l = self._listOfMonth[self.month-1].get_listOfDays()[self.day-1].get_date_lun()
		month_l = self._listOfMonth[self.month-1].get_monthLunar().strip("\n")
		return "{}{}".format(day_l,month_l)


	def get_today_date_sol(self):
		day_s = self.day
		month_s = self._listOfMonth[self.month-1].get_month()
		year_s = self.year
		return "{}\n{}-{}".format(day_s,month_s,year_s)

	def get_today_date_num(self):
		return "{}-{}-{}".format(self.day,self.month,self.year)

	def get_date(self):
		"""return 3-uples of int (day,month,year)"""
		return (self.day,self.month,self.year)

class Month:

	def __init__(self):
		#list of all days in a month
		self._month = ""
		self._monthLunar = ""
		self._listOfDays = []

	def set_month(self,month):
		self._month = month

	def get_month(self):
		return self._month

	def set_monthLunar(self,month):
		self._monthLunar = month

	def get_monthLunar(self):
		return self._monthLunar

	def get_listOfDays(self):
		return self._listOfDays

	def add_day(self,day):
		#adding a Day
		self._listOfDays.append(day)


class Day:

	def __init__(self):
		self._dayName = None
		self._dateSolar = None  #String 
		self._dateLunar = None  #String calendrier arabe
		self._prayers = []		  #liste taille 6 dans l'ordre des prières
		#contient des elements Timer()

	def set_date_sol(self,date):
		self._dateSolar = date
	
	def set_date_lun(self,date):
		self._dateLunar = date
	
	def set_dayName(self,name):
		self._dayName = name
	
	def get_date_sol(self):
		return self._dateSolar
	
	def get_date_lun(self):
		return self._dateLunar

	def get_dayName(self):
		return self._dayName

	def get_prayers(self):
		return self._prayers

	def set_prayers(self,hours):
		self._prayers.append(hours)
	
	def __str__(self):
		disp = ""
		for i in range(len(self._prayers)):
			disp += prayerNames[i] + " : " + self._prayers[i] + "\n"
		return disp

class Time:

	def __init__(self):
		self.hour = 0
		self.minute = 0

	def set_hour(self,h):
		self.hour = h

	def set_min(self,m):
		self.minute = m

	def get_hour(self):
		return self.hour

	def get_min(self):
		return self.minute

	def str_time(self):
		return "{:02d} : {:02d}".format(self.hour,self.minute)

	#date => str => "%d-%m-%Y %H:%M"
	def convert_to_epoch(self,date):
		p = "%d-%m-%Y %H : %M"
		if self.hour == 0:
			d = date.split("-")
			d[0] = str(int(d[0])+1)
			date = "-".join(d)
		date = date + " " +self.str_time()
		t = int(tm.mktime(tm.strptime(date,p)))
		return t

	

