# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'C:\Users\zakariya\Dropbox\Khalil Pray Time\unused\hissa3.ui'
#
# Created by: PyQt5 UI code generator 5.8.2
#
# WARNING! All changes made in this file will be lost!
stop = -1

from pri import *
import calendar
from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow,cal):
        MainWindow.setObjectName("MainWindow")
        MainWindow.setWindowModality(QtCore.Qt.NonModal)
        MainWindow.resize(1920, 1080)
        self.cal = cal
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setAutoFillBackground(True)
        self.centralwidget.setObjectName("centralwidget")
        self.centralwidget.setStyleSheet("QWidget {background-image:url(\"b.png\"); background-position: center; }")
        self.gridLayout = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout.setContentsMargins(5, 5, 5, 5)
        self.gridLayout.setObjectName("gridLayout")
        self.asr = QtWidgets.QLabel(self.centralwidget)
        self.asr.setMaximumSize(QtCore.QSize(300, 65))
        self.asr.setAlignment(QtCore.Qt.AlignCenter)
        self.asr.setObjectName("asr")
        font = QtGui.QFont()
        font.setFamily("orange juice")
        font.setPointSize(40)
        font.setBold(True)
        font.setWeight(75)
        self.asr.setFont(font)
        self.asr.setStyleSheet("QWidget {background-image:url(\"\"); background-position: center; }")
        self.gridLayout.addWidget(self.asr, 11, 2, 1, 1)
        self.dateLunar = QtWidgets.QLabel(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Maximum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.dateLunar.sizePolicy().hasHeightForWidth())
        self.dateLunar.setSizePolicy(sizePolicy)
        self.dateLunar.setMaximumSize(QtCore.QSize(300, 65))
        self.dateLunar.setAlignment(QtCore.Qt.AlignCenter)
        self.dateLunar.setObjectName("dateLunar")
        self.dateLunar.setStyleSheet("QWidget {background-image:url(\"\"); background-position: center; }")
        font = QtGui.QFont()
        font.setFamily("URW Gothic")
        font.setPointSize(20)
        font.setBold(True)
        font.setWeight(75)
        self.dateLunar.setFont(font)
        self.gridLayout.addWidget(self.dateLunar, 0, 2, 1, 1)
        self.maghreb = QtWidgets.QLabel(self.centralwidget)
        self.maghreb.setMaximumSize(QtCore.QSize(300, 65))
        self.maghreb.setAlignment(QtCore.Qt.AlignCenter)
        self.maghreb.setObjectName("maghreb")
        font = QtGui.QFont()
        font.setFamily("orange juice")
        font.setPointSize(40)
        font.setBold(True)
        font.setWeight(75)
        self.maghreb.setFont(font)
        self.maghreb.setStyleSheet("QWidget {background-image:url(\"\"); background-position: center; }")
        self.gridLayout.addWidget(self.maghreb, 12, 2, 1, 1)
        self.fajr = QtWidgets.QLabel(self.centralwidget)
        self.fajr.setMaximumSize(QtCore.QSize(300, 65))
        self.fajr.setAlignment(QtCore.Qt.AlignCenter)
        self.fajr.setObjectName("fajr")
        font = QtGui.QFont()
        font.setFamily("orange juice")
        font.setPointSize(40)
        font.setBold(True)
        font.setWeight(75)
        self.fajr.setFont(font)
        self.fajr.setStyleSheet("QWidget {background-image:url(\"\"); background-position: center; }")
        self.gridLayout.addWidget(self.fajr, 11, 0, 1, 1)
        self.ichaa = QtWidgets.QLabel(self.centralwidget)
        self.ichaa.setMaximumSize(QtCore.QSize(300, 65))
        self.ichaa.setAlignment(QtCore.Qt.AlignCenter)
        self.ichaa.setObjectName("ichaa")
        font = QtGui.QFont()
        font.setFamily("orange juice")
        font.setPointSize(40)
        font.setBold(True)
        font.setWeight(75)
        self.ichaa.setFont(font)
        self.ichaa.setStyleSheet("QWidget {background-image:url(\"\"); background-position: center; }")
        self.gridLayout.addWidget(self.ichaa, 13, 2, 1, 1)
        self.timeLeft = QtWidgets.QLabel(self.centralwidget)
        self.timeLeft.setMaximumSize(QtCore.QSize(300, 65))
        self.timeLeft.setAlignment(QtCore.Qt.AlignCenter)
        self.timeLeft.setObjectName("timeLeft")
        font = QtGui.QFont()
        font.setFamily("orange juice")
        font.setPointSize(40)
        font.setBold(True)
        font.setWeight(75)
        self.timeLeft.setFont(font)
        self.timeLeft.setStyleSheet("QWidget {background-image:url(\"\"); background-position: center; }")
        self.gridLayout.addWidget(self.timeLeft, 12, 1, 1, 1)
        self.dhor = QtWidgets.QLabel(self.centralwidget)
        self.dhor.setMaximumSize(QtCore.QSize(300, 65))
        self.dhor.setAlignment(QtCore.Qt.AlignCenter)
        self.dhor.setObjectName("dhor")
        font = QtGui.QFont()
        font.setFamily("orange juice")
        font.setPointSize(40)
        font.setBold(True)
        font.setWeight(75)
        self.dhor.setFont(font)
        self.dhor.setStyleSheet("QWidget {background-image:url(\"\"); background-position: center; }")
        self.gridLayout.addWidget(self.dhor, 13, 0, 1, 1)
        self.chourouq = QtWidgets.QLabel(self.centralwidget)
        self.chourouq.setMaximumSize(QtCore.QSize(300, 65))
        self.chourouq.setAlignment(QtCore.Qt.AlignCenter)
        self.chourouq.setObjectName("chourouq")
        font = QtGui.QFont()
        #import ipdb;ipdb.set_trace()
        font.setFamily("orange juice")
        font.setPointSize(40)
        font.setBold(True)
        font.setWeight(75)
        self.chourouq.setFont(font)
        self.chourouq.setStyleSheet("QWidget {background-image:url(\"\"); background-position: center; }")
        self.gridLayout.addWidget(self.chourouq, 12, 0, 1, 1)
        self.hour = QtWidgets.QLabel(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Maximum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.hour.sizePolicy().hasHeightForWidth())
        self.hour.setSizePolicy(sizePolicy)
        self.hour.setMaximumSize(QtCore.QSize(300, 65))
        self.hour.setAlignment(QtCore.Qt.AlignCenter)
        self.hour.setObjectName("hour")
        font = QtGui.QFont()
        font.setFamily("orange juice")
        font.setPointSize(40)
        font.setBold(True)
        font.setWeight(75)
        self.hour.setFont(font)
        self.hour.setStyleSheet("QWidget {background-image:url(\"\"); background-position: center; }")
        self.gridLayout.addWidget(self.hour, 0, 1, 1, 1)
        self.dateSolar = QtWidgets.QLabel(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Maximum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.dateSolar.sizePolicy().hasHeightForWidth())
        self.dateSolar.setSizePolicy(sizePolicy)
        self.dateSolar.setMaximumSize(QtCore.QSize(300, 65))
        self.dateSolar.setAlignment(QtCore.Qt.AlignCenter)
        self.dateSolar.setObjectName("dateSolar")
        font = QtGui.QFont()
        font.setFamily("URW Gothic")
        font.setPointSize(20)
        font.setBold(True)
        font.setWeight(75)
        self.dateSolar.setFont(font)
        self.dateSolar.setStyleSheet("QWidget {background-image:url(\"\"); background-position: center; }")
        self.gridLayout.addWidget(self.dateSolar, 0, 0, 1, 1)
        MainWindow.setCentralWidget(self.centralwidget)
        self.pray_lab = [self.fajr,self.chourouq,self.dhor,self.asr,self.maghreb,self.ichaa]
        p = QtGui.QPalette()
        p.setColor(QtGui.QPalette.WindowText,QtGui.QColor(255,255,255))
        for lab in self.pray_lab:
            lab.setPalette(p)
        self.hour.setPalette(p)
        self.dateSolar.setPalette(p)
        self.dateLunar.setPalette(p)
        self.timeLeft.setPalette(p)

    def disp_prayers(self):
        paryers = self.cal.get_today_prayer()
        self.fajr.setText(paryers[0].str_time())
        self.chourouq.setText(paryers[1].str_time())
        self.dhor.setText(paryers[2].str_time())
        self.asr.setText(paryers[3].str_time())
        self.maghreb.setText(paryers[4].str_time())
        self.ichaa.setText(paryers[5].str_time())

    def disp_date(self):
        self.dateSolar.setText(self.cal.get_today_date_sol())
        self.dateLunar.setText(self.cal.get_today_date_lun())

    def disp_countdown(self,count):
        count = str(dt.timedelta(seconds=count))
        self.timeLeft.setText(count)

    def get_curr_time_lab(self):
        return self.hour

    def get_cal(self):
        return self.cal

    def get_pray_lab(self):
        return self.pray_lab


class DisplayTime(Thread): 

    def __init__(self,currentTime):
        Thread.__init__(self)
        self.currentTime = currentTime

    def run(self):
        while stop < 0:
            time = dt.datetime.now().strftime('%H:%M:%S')
            self.currentTime.setText(time)
            tm.sleep(1)


class Timer(Thread):

    def __init__(self,gui):
        Thread.__init__(self)
        #listOfTimes sera les heures de priere en Time
        self.gui = gui
        self.listOfTimes = self.gui.get_cal().get_today_prayer()
        self.date = self.gui.get_cal().get_today_date_num()
        self.index_next_prayer = None

    #retourne l'indice du temps prochain dans listOfTimes
    def find_next_time(self):
        i = 0
        temp_time = tm.time()
        flag = True
        while i < len(self.listOfTimes) and  flag:
            time = self.listOfTimes[i].convert_to_epoch(self.date)
            if temp_time < time:
                flag = False
            else:
                i += 1
        if i < len(self.listOfTimes):
            #return index of next prayer
            self.index_next_prayer = i
        else:
            self.index_next_prayer = None


    def highlight_prayer(self,r,g,b):
        p = QtGui.QPalette()
        p.setColor(QtGui.QPalette.WindowText,QtGui.QColor(r,g,b))
        self.gui.get_pray_lab()[self.index_next_prayer].setPalette(p)

    def run(self):
        while stop < 0:    
            self.find_next_time()
            if self.index_next_prayer != None:
                pray_time = self.listOfTimes[self.index_next_prayer].convert_to_epoch(self.date)
                #self.highlight_prayer(178,166,96)
                self.highlight_prayer(209,173,0)
                countdown = pray_time - int(tm.time())
                while(countdown >= 0 and stop < 0):
                    self.gui.disp_countdown(countdown)
                    countdown = pray_time - int(tm.time())
                self.highlight_prayer(255,255,255)

class UpdateGUI(Thread):
    
    def __init__(self,gui):
        Thread.__init__(self)
        self.gui = gui
        #get le dernier elem
        self.lastPrayer = self.gui.get_cal().get_today_prayer()[-1]
        self.date = self.gui.get_cal().get_today_date_num()
        self.set = False
        self.update()


    def update(self):
        self.gui.get_cal().set_today_date()
        self.gui.disp_prayers()
        self.gui.disp_date()

    def run(self):
        while stop < 0:
            wait_time = self.lastPrayer.convert_to_epoch(self.date) - tm.time()
            if wait_time == 0:
                self.update()
            tm.sleep(1)

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    c = Calendar()
    for i in range(1,13):  
        Parser(c,"./monthCal/"+str(i)+".csv")
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow,c)
    disp = DisplayTime(ui.get_curr_time_lab())
    updt = UpdateGUI(ui)
    timer = Timer(ui)
    MainWindow.showFullScreen()
    disp.start()
    updt.start()
    timer.start()
    stop = app.exec_()
    sys.exit(stop)
